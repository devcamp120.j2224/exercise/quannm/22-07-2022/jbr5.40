package com.devcamp.jbr540.jbr540.service;

import java.util.ArrayList;

import com.devcamp.jbr540.jbr540.model.Customer;

public class CustomerService {
    public ArrayList<Customer> getCustomer() {
        ArrayList<Customer> lCustomers = new ArrayList<>();
        Customer customer1 = new Customer(1, "QuanNM", 10);
        Customer customer2 = new Customer(2, "BoiHB", 15);
        Customer customer3 = new Customer(3, "ThuNHM", 13);
        lCustomers.add(customer1);
        lCustomers.add(customer2);
        lCustomers.add(customer3);
        return lCustomers;
    }
}

package com.devcamp.jbr540.jbr540.service;

import java.util.ArrayList;

import com.devcamp.jbr540.jbr540.model.Customer;
import com.devcamp.jbr540.jbr540.model.Invoice;

public class InvoiceService {
    public ArrayList<Invoice> getAllInvoice() {
        ArrayList<Invoice> lInvoices = new ArrayList<>();
        CustomerService lCustomerService = new CustomerService();
        ArrayList<Customer> lCustomers = lCustomerService.getCustomer();
        Invoice invoice1 = new Invoice(1, lCustomers.get(0), 500);
        Invoice invoice2 = new Invoice(2, lCustomers.get(1), 800);
        Invoice invoice3 = new Invoice(3, lCustomers.get(2), 600);
        lInvoices.add(invoice1);
        lInvoices.add(invoice2);
        lInvoices.add(invoice3);
        return lInvoices;
    }
}

package com.devcamp.jbr540.jbr540.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr540.jbr540.model.Invoice;
import com.devcamp.jbr540.jbr540.service.InvoiceService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class InvoiceController {
    @GetMapping("/invoices")
    public ArrayList<Invoice> getListInvoice() {
        return new InvoiceService().getAllInvoice();
    }
}
